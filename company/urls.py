from django.urls import path

from .views import IndexView,CreateView,EditView,DeleteView

urlpatterns=[
    path('',IndexView,name='index'),
    path('create/',CreateView,name='create'),
    path('edit/<int:id>/',EditView,name='edit'),
    path('delete/<int:id>/',DeleteView,name='delete'),
]
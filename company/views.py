from django.shortcuts import redirect, render
from .forms import CompanyProfileForm
from .models import CompanyProfile
from django.contrib import messages

def IndexView(request,*args,**kwargs):
    template = 'company/index.html'
    if request.method == 'GET':
        queryset = CompanyProfile.objects.all()
        return render(request,template,{'queryset':queryset})

    else:
        return redirect('/company')

def CreateView(request, *args,**kwargs):
    template = 'company/create.html'
    if request.method == 'POST':
        form = CompanyProfileForm(request.POST,request.FILES or None)
        if form.is_valid():
            try:
                form.save()
                messages.success(request, 'Data Created Successfully')
            except Exception as e:
                print(str(e))
            return redirect('/company')
        else:
            print(form.errors)
    else:
       form = CompanyProfileForm()
    return render(request,template,{"form":form})

def EditView(request,id,*args,**kwargs):
    template = 'company/edit.html'
    query = CompanyProfile.objects.get(id=id)
    if request.method == 'GET':
        form = CompanyProfileForm(instance=query)
        return render(request,template,{'form':form,'id':id})

    if request.method == 'POST':
        form = CompanyProfileForm(request.POST,request.FILES,instance = query)
        if form.is_valid():
            form.save()
            messages.success(request, 'Data Updated Successfully')
        else:
            print(form.errors)
        return redirect('/company')
    return redirect('/company')

def DeleteView(request,id,*args,**kwargs):
    query = CompanyProfile.objects.get(id=id)
    query.delete()
    messages.success(request, 'Data Deleted Successfgully')
    return redirect('/company')
from company.forms import CompanyProfileForm
from company.models import CompanyProfile
from django.contrib import admin

class CompanyProfileAdmin(admin.ModelAdmin):
    form = CompanyProfileForm
    
admin.site.register(CompanyProfile,CompanyProfileAdmin)
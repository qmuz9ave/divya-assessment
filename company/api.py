from company.serializers import CompanyProfileSerializer
from rest_framework import viewsets
from .models import CompanyProfile

class CompanyProfileViewSet(viewsets.ModelViewSet):
    queryset = CompanyProfile.objects.all()
    serializer_class = CompanyProfileSerializer
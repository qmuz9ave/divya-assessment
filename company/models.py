from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid
from django.core.validators import FileExtensionValidator

ALLOWED_DOCUMENT_EXTENSIONS = [
    'pdf',
    'docs',
    'png',
    'jpeg',
    'jpg',
]

def upload_document(instance,filename):
    name, ext = filename.split('.')
    return "documents/{}.{}".format(uuid.uuid1(), ext)


class CompanyProfile(models.Model):
    company_name = models.CharField(max_length=100,unique = True, verbose_name=_('Company Name'))
    company_address = models.CharField(max_length=100,verbose_name=_('Company Address'))
    company_email = models.EmailField(unique=True)
    company_registered_document = models.FileField(upload_to=upload_document,validators=[FileExtensionValidator(ALLOWED_DOCUMENT_EXTENSIONS)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return str(self.company_name)

    class Meta:
        verbose_name = _('Company Profile')
        verbose_name_plural = _('Company Profile')



